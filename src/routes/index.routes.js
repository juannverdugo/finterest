const Image = require('./../models/Image');
const path = require('path');
const {unlink} = require('fs-extra');

module.exports = (app) => {

    app.get('/', async (req, res) => {
       const images = await Image.find();
       console.log(images);
       res.render('index', {images});
    });

    app.get('/upload', (req, res) => {
        res.render('upload');
    });

    app.post('/upload', async (req, res) => {
        const {title, description} = req.body;
        const {filename, originalname, mimetype, size} = req.file;
        const image = await new Image();

        image.title = title;
        image.description = description;
        image.filename = filename;
        image.path = '/img/uploads/' + filename;
        image.originalname = originalname;
        image.mimetype = mimetype;
        image.size = size;

        await image.save();

        res.render('upload');
    });

    app.get('/image/:id', async (req, res) => {
        const {id} = req.params;
        const image =await Image.findById(id);
        res.render('profile', { image })
    });

    app.get('/image/:id/delete', async (req, res) => {
        const { id } = req.params;
        const image = await Image.findByIdAndDelete(id);
        await unlink(path.resolve('./src/public/' + image.path));
        res.redirect('/');
    });
}